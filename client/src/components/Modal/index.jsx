import React from "react";
import PropTypes from "prop-types";
import BModal from "react-bootstrap/Modal";

export default function Modal({ show, onHide, title }) {
  return (
    <BModal
      show={show}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <BModal.Header closeButton>
        <BModal.Title id="contained-modal-title-vcenter">{title}</BModal.Title>
      </BModal.Header>
    </BModal>
  );
}

Modal.propTypes = {
  show: PropTypes.bool,
  onHide: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

Modal.defaultProps = {
  show: false,
};
