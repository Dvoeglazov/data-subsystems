import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import InputMask from "react-input-mask";
import { useForm, Controller } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import Joi from "joi";

// components
import Modal from "components/Modal";

// api
import api from "api";

const schema = Joi.object({
  cardNumber: Joi.string()
    .pattern(/^\d{16}$/)
    .required(),
  expirationDate: Joi.string()
    .pattern(/(^0?[1-9]|^1[0-2])\/(\d{4}$)/)
    .required(),
  cvv: Joi.string()
    .pattern(/^\d{3}$/)
    .required(),
  amount: Joi.number().positive().required(),
});

export default function App() {
  // modal
  const [show, setShow] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // form
  const {
    control,
    handleSubmit,
    reset,
    formState: { isSubmitting, isValid },
  } = useForm({
    resolver: joiResolver(schema),
    defaultValues: {
      cardNumber: "",
      expirationDate: "",
      cvv: "",
      amount: "",
    },
    mode: "onBlur",
  });

  const onSubmit = async (data) => {
    // console.log(data);
    try {
      await api.post("payment", {
        ...data,
      });

      setModalTitle("Message sent");
      handleShow();
      reset({ cardNumber: "", expirationDate: "", cvv: "", amount: "" });
    } catch (error) {
      console.error("onSubmit error", error);
      setModalTitle("Error!");
      handleShow();
    }
  };

  return (
    <div className="app py-5">
      <Container>
        <Row className="justify-content-center">
          <Col xs={6}>
            <Form>
              <Form.Group className="mb-3" controlId="cardNumber">
                <Form.Label>Card Number</Form.Label>

                <Controller
                  control={control}
                  name="cardNumber"
                  render={({
                    field: { onChange, onBlur, value },
                    fieldState: { error },
                  }) => (
                    <InputMask
                      mask="9999999999999999"
                      maskChar="*"
                      value={value}
                      onChange={onChange}
                      onBlur={onBlur}
                    >
                      {(inputProps) => (
                        <Form.Control
                          {...inputProps}
                          name="cardNumber"
                          placeholder="Card Number"
                          isInvalid={!!error}
                        />
                      )}
                    </InputMask>
                  )}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="expirationDate">
                <Form.Label>Expiration Date</Form.Label>

                <Controller
                  control={control}
                  name="expirationDate"
                  render={({
                    field: { onChange, onBlur, value },
                    fieldState: { error },
                  }) => (
                    <InputMask
                      mask="99/9999"
                      maskChar="*"
                      value={value}
                      onChange={onChange}
                      onBlur={onBlur}
                    >
                      {(inputProps) => (
                        <Form.Control
                          {...inputProps}
                          placeholder="MM/YYYY"
                          isInvalid={!!error}
                        />
                      )}
                    </InputMask>
                  )}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="cvv">
                <Form.Label>CVV</Form.Label>
                <Controller
                  control={control}
                  name="cvv"
                  render={({
                    field: { onChange, onBlur, value },
                    fieldState: { error },
                  }) => (
                    <InputMask
                      mask="999"
                      maskChar="*"
                      value={value}
                      onChange={onChange}
                      onBlur={onBlur}
                      isInvalid={!!error}
                    >
                      {(inputProps) => (
                        <Form.Control {...inputProps} placeholder="CVV" />
                      )}
                    </InputMask>
                  )}
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="amount">
                <Form.Label>Amount</Form.Label>
                <Controller
                  control={control}
                  name="amount"
                  render={({
                    field: { onChange, onBlur, value },
                    fieldState: { error },
                  }) => (
                    <Form.Control
                      value={value}
                      onChange={onChange}
                      onBlur={onBlur}
                      isInvalid={!!error}
                      placeholder="Amount"
                      onKeyPress={(event) => {
                        if (!/\d/.test(event.key)) {
                          event.preventDefault();
                        }
                      }}
                    />
                  )}
                />
              </Form.Group>

              <Button
                variant="primary"
                disabled={!isValid}
                onClick={
                  !isSubmitting || isValid ? handleSubmit(onSubmit) : null
                }
              >
                Submit
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>

      <Modal show={show} onHide={handleClose} title={modalTitle} />
    </div>
  );
}
