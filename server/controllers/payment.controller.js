const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { paymentService } = require("../services");

const createPayment = catchAsync(async (req, res) => {
  const { id, amount } = await paymentService.createPayment(req.body);
  res.status(httpStatus.CREATED).send({ id, amount });
});

const getAllPayments = catchAsync(async (req, res) => {
  const result = await paymentService.getAllPayments();
  res.send(result);
});

module.exports = {
  createPayment,
  getAllPayments,
};
