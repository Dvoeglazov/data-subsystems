const { Payment } = require("../models");

/**
 * Create a payment
 * @param {Object} body
 * @returns {Promise<Payment>}
 */
const createPayment = async (body) => {
  return Payment.create(body);
};

/**
 * Query for payments
 * @returns {Promise<QueryResult>}
 */
const getAllPayments = async () => {
  const payments = await Payment.find();
  return payments;
};

module.exports = {
  createPayment,
  getAllPayments,
};
