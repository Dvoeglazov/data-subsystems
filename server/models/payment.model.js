const mongoose = require("mongoose");
const validator = require("validator");
const { toJSON } = require("./plugins");

const paymentSchema = mongoose.Schema(
  {
    cardNumber: {
      type: String,
      required: true,
      trim: true,
      validate(value) {
        if (!validator.matches(value, /^\d{16}$/)) {
          throw new Error();
        }
      },
    },
    expirationDate: {
      type: String,
      required: true,
      trim: true,
      validate(value) {
        if (!validator.matches(value, /(^0?[1-9]|^1[0-2])\/(\d{4}$)/)) {
          throw new Error();
        }
      },
    },
    cvv: {
      type: String,
      required: true,
      trim: true,
      validate(value) {
        if (!validator.matches(value, /^\d{3}$/)) {
          throw new Error();
        }
      },
    },
    amount: {
      type: Number,
      required: true,
      trim: true,
      validate(value) {
        if (
          !validator.matches(`${value}`, /\d/) ||
          !(Number.isInteger(Number(value)) && Number(value) > 0)
        ) {
          throw new Error();
        }
      },
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
paymentSchema.plugin(toJSON);

/**
 * @typedef Payment
 */
const Payment = mongoose.model("Payment", paymentSchema);

module.exports = Payment;
