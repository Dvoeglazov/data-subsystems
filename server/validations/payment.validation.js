const Joi = require("joi");

const createPayment = {
  body: Joi.object().keys({
    cardNumber: Joi.string()
      .pattern(/^\d{16}$/)
      .required(),
    expirationDate: Joi.string()
      .pattern(/(^0?[1-9]|^1[0-2])\/(\d{4}$)/)
      .required(),
    cvv: Joi.string()
      .pattern(/^\d{3}$/)
      .required(),
    amount: Joi.number().positive().required(),
  }),
};

module.exports = {
  createPayment,
};
